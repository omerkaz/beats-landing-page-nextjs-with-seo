import { Roboto_Slab, Open_Sans, Roboto } from 'next/font/google'

const roboto = Roboto({
  subsets: ['latin'],
  weight: '400',
})
const roboto_slab = Roboto_Slab({ subsets: ['latin'] })
const open_sans = Open_Sans({ subsets: ['latin'] })

export { roboto_slab, open_sans, roboto }
