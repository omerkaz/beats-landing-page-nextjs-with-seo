import Image from 'next/image'
import Button from '@/components/ui/Button'
import Heading from '@/components/ui/Heading'
import SubsetHeading from '@/components/ui/SubsetHeading'
import Section1 from '@/components/sections/Section1'
export default function Home() {
  return (
    <main className='flex min-h-screen flex-col items-center justify-between p-24'>
      <Section1></Section1>
      {/* <Heading>Apple W1 Chip</Heading>
      <SubsetHeading>CUSTOM COMFORT</SubsetHeading>
      <Button>READ MORE</Button> */}
    </main>
  )
}
