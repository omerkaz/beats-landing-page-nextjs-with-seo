import React from 'react'
import { roboto_slab } from '@/assets/font/font'

interface IHeadingProps extends React.HTMLProps<HTMLHeadingElement> {
  children: React.ReactNode
}

const Heading = ({ children, ...props }: IHeadingProps) => {
  return (
    <h1 className={`${roboto_slab.className} text-5xl`} {...props}>
      {children}
    </h1>
  )
}

export default Heading
