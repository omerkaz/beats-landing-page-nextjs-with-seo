import React from 'react'

interface ISubsetHeadingProps extends React.HTMLProps<HTMLHeadingElement> {
  children: React.ReactNode
}

const SubsetHeading = ({ children, ...props }: ISubsetHeadingProps) => {
  return (
    <h1 className='text-lg tracking-widest' {...props}>
      {children}
    </h1>
  )
}

export default SubsetHeading
