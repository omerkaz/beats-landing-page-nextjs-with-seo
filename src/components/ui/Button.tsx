import React from 'react'
import { open_sans } from '@/assets/font/font'

interface IButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  children: React.ReactNode
}

const Button = ({ children, ...props }: IButtonProps) => {
  return (
    <button
      {...props}
      className={`${open_sans.className} rounded-full bg-red-500 px-8 py-3 font-bold text-zinc-50 hover:bg-red-600`}
    >
      {children}
    </button>
  )
}

export default Button
