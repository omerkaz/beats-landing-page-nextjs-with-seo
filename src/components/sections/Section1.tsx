import React from 'react'
import Button from '../ui/Button'
import Image from 'next/image'
import Headphone from '@/assets/images/headphone.png'
import Brand from '@/assets/images/brand.png'

const Section1 = () => {
  return (
    <section className='w-screen'>
      <div className='relative m-auto sm:w-screen lg:w-8/12'>
        <Image src={Brand} alt='Brand' width={210} height={103} />
        <Image src={Headphone} alt='Headphone' width={850} height={850} />
        <div className='relative float-right w-5/6 md:w-4/6 lg:w-1/2'>
          <h1>Beats Solo3 </h1>
          <h2>Wireless</h2>
          <p className='italic'>
            With up to 40 hours of battery life, Beats Solo3 Wireless is your
            perfect everyday headphone. With Fast Fuel, a 5-minute charge gives
            you 3 hours of playback.
          </p>
          <Button>$299.95 Buy</Button>
        </div>
      </div>
    </section>
  )
}

export default Section1
